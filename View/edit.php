<?php
include_once './H_header.php';
include_once '../vendor/autoload.php';


use comment\file\comment;

$obj=new comment();
if(isset($_GET['id']) && !empty($_GET['id']))
{
    $data=$obj->prepare($_GET)->show();
}


//print_r($data);
?>


        <html>
            <head>
                <title></title>
                <style>
            *{
                margin: 0px;
                padding: 0px;
            }
            #form{
                width: 900px;
                height: 400px;
                background:  #ccccff;
                margin: 0 auto;
            }
            table{
                margin: 0 auto;
                border: 1px solid #ff0000;
                padding: 20px;
            }
            input[type="submit"]
            {
               line-height: 40px;
               width: 100px;
               height: 40px;
               float: right;
            }
            input[type="reset"]
            {
               line-height: 40px;
               width: 100px;
               height: 40px;
               float: right;
            }
            input[type="submit"]:hover
            {
                background: #00ff00;
                color: #ffffff;
            }
            input[type="reset"]:hover
            {
                background: #00ff00;
                color: #ffffff;
            }
        </style>
      </head>
               
    <body>
        <div id="main">
            <div id="table">
                <form action="update.php" method="post" id="form">
                    <br><br><br>
                    <table>
                         <tr>
                            <td colspan="2">
                                <?php
                                 if(isset(  $_SESSION['update_msg']))
                                 {
                                     echo   $_SESSION['update_msg'];
                                     unset(  $_SESSION['update_msg']);
                                 }
                                ?>
                            </td>
                        </tr>
                         <tr>
                             <td><label>First Name </label></td>
                             <td><input type="text" name="fname" value="<?php if(isset($data['firstName'])){ echo $data['firstName'];} ?>"></td>
                         </tr>
                          <tr>
                              <td><label>Last Name </label></td>
                              <td> <input type="text" name="lname" value="<?php if(isset($data['lastName'])){ echo $data['lastName'];} ?>"></td>
                         </tr>

                          <tr>
                              <td><label>Comment</label></td>
                             <td> <textarea rows="2" cols="50" name="comment"><?php if(isset($data['comment'])){ echo $data['comment'];} ?></textarea></td>
                         </tr>
                          <tr>
                              <td colspan="2"><input type="submit" value="Update"></td>
                         </tr>
                         <tr>
                             <td colspan="2" style="text-align: center;"></td>
                         </tr>
                         <tr>
                             <td colspan="2"> <a href="index.php">View List</a></td>
                         </tr>
                     </table>
                  </form>
            </div>
           
        </div>
    </body>

        </html>

