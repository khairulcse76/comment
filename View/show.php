<?php
include_once './H_header.php';
include_once '../vendor/autoload.php';

use comment\file\comment;

$obj=new comment();
$data=$obj->prepare($_GET)->show();

//print_r($data);

?>
<br>
<br>
<table style=" margin: 0 auto; border: 2px solid #ff0000; padding: 20px;">
    <tr>
        <th>ID</th>
        <td>:</td>
        <td><?php echo $data['id'] ?></td>
    </tr>
     <tr>
        <th>Name</th>
        <td>:</td>
        <td><?php echo $data['firstName']." ".$data['lastName'] ?></td>
    </tr>
     <tr>
        <th>Comment</th>
        <td>:</td>
        <td><?php echo $data['comment'] ?></td>
    </tr>
     <tr>
         <td colspan="3"><a href="index.php">BACK TO LIST</a></td>
    </tr>
</table>