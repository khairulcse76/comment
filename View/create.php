<?php
include_once './H_header.php';
session_start();
?>

        <html>
            <head>
                <title></title>
                <style>
            *{
                margin: 0px;
                padding: 0px;
            }
            #form{
                width: 900px;
                height: 400px;
                background:  #ccccff;
                margin: 0 auto;
            }
            table{
                margin: 0 auto;
                border: 1px solid #ff0000;
                padding: 20px;
            }
            input[type="submit"]
            {
               line-height: 40px;
               width: 100px;
               height: 40px;
               float: right;
            }
            input[type="reset"]
            {
               line-height: 40px;
               width: 100px;
               height: 40px;
               float: right;
            }
            input[type="submit"]:hover
            {
                background: #00ff00;
                color: #ffffff;
            }
            input[type="reset"]:hover
            {
                background: #00ff00;
                color: #ffffff;
            }
        </style>
      </head>
               
    <body>
        <div id="main">
            <div id="table">
                <form action="store.php" method="post" id="form">
                    <br><br><br>
                    <table>
                       
                         <tr>
                             <td><label>First Name <span style="color: #ff0000">*</span></label></td>
                             <td><input type="text" name="fname" placeholder="First name">
                              <?php
                                if(isset( $_SESSION['fname_req']))
                                {
                                    echo  $_SESSION['fname_req'];
                                    unset( $_SESSION['fname_req']);
                                }
                             ?>
                             </td>
                         </tr>
                          <tr>
                              <td><label>Last Name <span style="color: #ff0000">*</span></label></td>
                             <td> <input type="text" name="lname" placeholder="Last name">
                             <?php
                                if(isset( $_SESSION['lname_req']))
                                {
                                    echo  $_SESSION['lname_req'];
                                    unset( $_SESSION['lname_req']);
                                }
                             ?>
                             </td>
                         </tr>

                          <tr>
                              <td><label>Comment <span style="color: #ff0000">*</span></label></td>
                             <td> <textarea rows="4" cols="50" name="comment"></textarea>
                              <?php
                                if(isset( $_SESSION['cmt_req']))
                                {
                                    echo  $_SESSION['cmt_req'];
                                    unset( $_SESSION['cmt_req']);
                                }
                             ?>
                             </td>
                         </tr>
                          <tr>
                              <td colspan="2"><input type="submit" value="Submit"><input type="reset" name="reset"></td>
                         </tr>
                         <tr>
                             <td colspan="2" style="text-align: center;">
                                 <?php
                                    if(isset($_SESSION['store_err']))
                                    {
                                        echo $_SESSION['store_err'];
                                        unset($_SESSION['store_err']);
                                        unset($_SESSION['store_err']);
                                    }
                                 ?>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2"> <a href="index.php">View List</a></td>
                         </tr>
                     </table>
                  </form>
            </div>
           
        </div>
    </body>

        </html>


<?php
include_once './footer.php';
?>
